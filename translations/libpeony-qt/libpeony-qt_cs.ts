<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DiscControl</name>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="453"/>
        <source> is busy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="492"/>
        <source>is busy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="539"/>
        <source> not support udf at present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="546"/>
        <source>unmount disc failed before udf format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="699"/>
        <source>DVD+RW udf format fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="731"/>
        <source>preparation failed before DVD-RW udf format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="37"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="56"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="38"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="57"/>
        <source>Orange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="39"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="58"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="40"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="59"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="41"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="60"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="42"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="61"/>
        <source>Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="43"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="62"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Label or color is duplicated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="143"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="156"/>
        <source>Rom size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="162"/>
        <source>Filesystem:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="172"/>
        <source>Disk name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="202"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="220"/>
        <source>Set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="221"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="234"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="235"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="348"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="548"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="548"/>
        <source>Device name cannot start with a decimal point, Please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="559"/>
        <source>Enter Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="576"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="675"/>
        <source>%1/sec, %2 remaining.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="675"/>
        <source>over one day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="677"/>
        <source>getting progress...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1252"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1252"/>
        <source>Block not existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1298"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="67"/>
        <source>Renaming &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="68"/>
        <source>Renaming failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="68"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="75"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="82"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="87"/>
        <source>Filename too long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="72"/>
        <source>Copying &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="74"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="81"/>
        <source>To &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="75"/>
        <source>Copying failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="79"/>
        <source>Moving &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="82"/>
        <source>Moving failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="86"/>
        <source>File operation error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="87"/>
        <source>The reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="119"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="156"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="117"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="118"/>
        <source>Skip All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="120"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="146"/>
        <source>Please enter a new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="157"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="397"/>
        <source>File operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="436"/>
        <source>starting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="413"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="541"/>
        <source>cancel all file operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="414"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="542"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="635"/>
        <source>canceling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="638"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="416"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="544"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="545"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1771"/>
        <source>Forcibly pulling out the device may cause data
 loss or device exceptions!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="739"/>
        <source>Other queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">İçeriği veya dosya adını en az birini seçin!</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="191"/>
        <source>Search Content...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="364"/>
        <source>Choose new application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="366"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="373"/>
        <source>apply now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="379"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="380"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="886"/>
        <source>Choose a custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="271"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="243"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="244"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="448"/>
        <source>symbolLink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="452"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="278"/>
        <source>Include:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="282"/>
        <source>Open with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="286"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="289"/>
        <source>Select multiple files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="277"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="295"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="296"/>
        <source>Space Usage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="307"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="315"/>
        <source>Time Create:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="308"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="316"/>
        <source>Time Modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="309"/>
        <source>Time Access:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="322"/>
        <source>Readonly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="323"/>
        <source>Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="332"/>
        <source>Property:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="604"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="713"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="927"/>
        <source>%1 (%2 Bytes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="920"/>
        <source>%1 Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="939"/>
        <source>%1 files, %2 folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1042"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1044"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1049"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1051"/>
        <source>Can&apos;t get remote file information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="99"/>
        <source>CPU Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="100"/>
        <source>CPU Core:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="101"/>
        <source>Memory Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="113"/>
        <source>User Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="114"/>
        <source>Desktop: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="122"/>
        <source>You should mount this volume first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="139"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="237"/>
        <source>Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="139"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="139"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="140"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="242"/>
        <source>Total Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="141"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="243"/>
        <source>Used Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="244"/>
        <source>Free Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="143"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="246"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="259"/>
        <source>Kylin Burner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="265"/>
        <source>Open with: 	</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="272"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="143"/>
        <source>connect to server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="168"/>
        <source>ip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="170"/>
        <source>port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="171"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="202"/>
        <source>Personal Collection server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="215"/>
        <source>add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="216"/>
        <source>delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="217"/>
        <source>connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="294"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="298"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="366"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="370"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="294"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="366"/>
        <source>ip input error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="298"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="370"/>
        <source>port input error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="465"/>
        <source>The login user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="474"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="481"/>
        <source>User&apos;s identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="482"/>
        <source>guest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="483"/>
        <source>Registered users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="498"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="499"/>
        <source>password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="500"/>
        <source>Remember the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="520"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="521"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="122"/>
        <source>Create Link to Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="148"/>
        <source>Create Link to...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="151"/>
        <source>Choose a Directory to Create Link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="218"/>
        <source>Create Link to Desktop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation type="unfinished">Dosya oluştur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="44"/>
        <source>Error not be handled correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultAcitonWidget</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="491"/>
        <source>No default app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="423"/>
        <source>No default app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="73"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="188"/>
        <source>Select the file you want to preview...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="179"/>
        <source>Can not preview this file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">Bu dosya önizlemesi görüntülenemiyor.</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="175"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="178"/>
        <source>File type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="194"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="205"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="212"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="198"/>
        <source>Create time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="202"/>
        <source>Modify time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="210"/>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="219"/>
        <source>File size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="226"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="229"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="237"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="238"/>
        <source>Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="240"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="241"/>
        <source>Computer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="286"/>
        <source>%1 (this computer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="293"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="334"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="335"/>
        <source>Can&apos;t get remote file information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="344"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="345"/>
        <source>%1 px</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="obsolete">Simge Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="304"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="304"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="obsolete">Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="557"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="557"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">Liste Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="327"/>
        <source>Add to bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">&amp;Yeni</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="624"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="676"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="678"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="679"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="289"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="380"/>
        <source>Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="299"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="390"/>
        <source>Open in New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="347"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="399"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="454"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="358"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="411"/>
        <source>Open with...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="373"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="448"/>
        <source>More applications...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="463"/>
        <source>Open %1 selected files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="511"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="608"/>
        <source>Empty File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="620"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="650"/>
        <source>View Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="670"/>
        <source>Sort By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="677"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="713"/>
        <source>Ascending Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="712"/>
        <source>Descending Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="680"/>
        <source>Orignal Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="731"/>
        <source>Folder First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="740"/>
        <source>Chinese First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="749"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="784"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="792"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="918"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="960"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1151"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="812"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1156"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="837"/>
        <source>Delete to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="888"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="931"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="942"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="995"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1032"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1070"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1119"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1211"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1231"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1241"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1231"/>
        <source>Peony File Labels Menu Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1263"/>
        <source>MultiSelect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="849"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="912"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1139"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="705"/>
        <source>Sort Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="726"/>
        <source>Sort Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1210"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">&amp;Kes</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="852"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="863"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="871"/>
        <source>Delete forever</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="879"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="951"/>
        <source>Reverse Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">&amp;Özellikler</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Yeniden Adlandır</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1095"/>
        <source>&amp;Clean the Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1143"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1143"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1164"/>
        <source>Clean All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1181"/>
        <source>Open Parent Folder in New Window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">Dizin Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="156"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="167"/>
        <source>Error in source or destination file path!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="193"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="220"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="319"/>
        <source>Please check whether the device has been removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="321"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="396"/>
        <source>File opening failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="336"/>
        <source>operation cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="obsolete">Dosya kopyalama</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="195"/>
        <source>Create folder %1 failed: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="199"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="464"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="819"/>
        <source>File copy error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="472"/>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="474"/>
        <source>File:%1 was not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="821"/>
        <source>Burn failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="obsolete">Dosya silme</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="78"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="104"/>
        <source>File delete error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="141"/>
        <source>Delete file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="144"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="566"/>
        <source>The password dialog box is canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="568"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="304"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="332"/>
        <source>folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="336"/>
        <location filename="../../libpeony-qt/file-info.cpp" line="344"/>
        <location filename="../../libpeony-qt/file-info.cpp" line="346"/>
        <source>file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="339"/>
        <source>text file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="280"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="282"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="284"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="286"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="244"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="308"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="320"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="325"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="296"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="297"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="309"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="319"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="315"/>
        <source>child(ren)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="303"/>
        <source>Symbol Link, </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="349"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="353"/>
        <source>Delete Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="358"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="360"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="356"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="354"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="124"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="140"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="155"/>
        <source>The opening mode of the %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="124"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="125"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="155"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="156"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="125"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="156"/>
        <source>No application is set to open file &quot;%1 %2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="130"/>
        <source>Still using the last opened application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="140"/>
        <source>known</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="143"/>
        <source>Open application is used by default:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="164"/>
        <source>You can search in the Software Center for an application that can open this file, or select an existing application on your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="186"/>
        <source>Other application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="188"/>
        <source>Select application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="234"/>
        <source>Always open the %1%2 file with this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="259"/>
        <source>Choose other application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="272"/>
        <source>Go to application center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="321"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="322"/>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="381"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="372"/>
        <source>Desktop files(*.desktop)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="379"/>
        <source>Select Open Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="380"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="168"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="294"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="513"/>
        <source>Execute Directly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="169"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="514"/>
        <source>Execute in Terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="172"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="299"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="518"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="190"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="332"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="551"/>
        <source>Open Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="190"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">Dosya mevcut değil, silinmiş veya başka bir yere taşınmış olabilir</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">%1 açılamadı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="142"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="265"/>
        <source>No Permission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="142"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="265"/>
        <source>File is not readable. Please check if file has read permisson.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="293"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="512"/>
        <source>By Default App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="298"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="517"/>
        <source>Launch Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="322"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="541"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="323"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="542"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="333"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="552"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="337"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="556"/>
        <source>Open App failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="338"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="557"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="351"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="356"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="567"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="352"/>
        <source>File original path not exist, are you deleted or moved it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="356"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="567"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="877"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="877"/>
        <source>Can not open the file, application is disabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="40"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="43"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="79"/>
        <source>Link file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="obsolete">Dosyayı bağla</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">Geçersiz taşıma işlemi, bir dosyanın kendisini taşıyamaz.</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="obsolete">Dosyayı taşı</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="obsolete">Dosya oluştur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="150"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="306"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="426"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="700"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1291"/>
        <source>Move file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="909"/>
        <source>Create file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="915"/>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="917"/>
        <source>File:%1 was not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1207"/>
        <source>Invalid Operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1258"/>
        <source>File delete error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1293"/>
        <source>Burn failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="obsolete">Dosya silme</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1260"/>
        <source>Invalid Operation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="45"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="54"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="72"/>
        <source>Do the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="95"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="101"/>
        <source>Unexpected error from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="63"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="302"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="294"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="339"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="194"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="202"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="227"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="obsolete">Lütfen diskin dolu olmadığından veya yazmaya karşı korumalı olmadığından veya dosyanın kullanılmadığından emin olun.</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="756"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="758"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="obsolete"> - Symbolik Link</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="190"/>
        <source>Warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="190"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="209"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="213"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="218"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="220"/>
        <source>Do not show again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="328"/>
        <source>Can&apos;t delete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="329"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="412"/>
        <source>File Operation is Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="413"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">Daha önce bir veya daha fazla dosya işlemi yürütüldü. Operasyonunuz tamamlanana kadar yürütmeyi bekleyecektir.</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="228"/>
        <source>File Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="256"/>
        <source>Time Access:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="249"/>
        <source>Time Modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="265"/>
        <source>Children Count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="235"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="242"/>
        <source>Time Created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="274"/>
        <source>Image resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="281"/>
        <source>color model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="347"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="394"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="395"/>
        <source>%1x%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="452"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="453"/>
        <source>%1 total, %2 hidden</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="obsolete">Dosyayı yeniden isimlendir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="74"/>
        <source>File Rename error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="90"/>
        <source>File Rename warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="176"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="206"/>
        <source>Rename file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="68"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="91"/>
        <source>trash:///</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="71"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="94"/>
        <source>Trash file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="74"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="102"/>
        <source>Can not trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="103"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="171"/>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="obsolete">Çöp dosyası</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="obsolete">Dosyayı geri al</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="159"/>
        <source>Untrash file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="68"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="425"/>
        <source>yyyy/MM/dd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="69"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="417"/>
        <source>HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="414"/>
        <source>AP hh:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="428"/>
        <source>yyyy-MM-dd</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::HeaderBar</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="901"/>
        <source>Spread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="902"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="903"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="355"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="384"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="476"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="480"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="474"/>
        <source>Copy Directory</source>
        <translation>&quot;&gt;Dizini Kopyala</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="187"/>
        <source>Login failed, unknown username or password error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="230"/>
        <source>Choose new application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="239"/>
        <source>apply now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="245"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="246"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="106"/>
        <source>How do you want to open %1%2 files ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="111"/>
        <source>Default open with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="130"/>
        <source>Other:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="171"/>
        <source>Choose other application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="187"/>
        <source>Go to application center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="82"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="161"/>
        <source>Target: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="139"/>
        <source>Group or User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="139"/>
        <source>Read and Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="139"/>
        <source>Readonly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="253"/>
        <source>(Current User)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="333"/>
        <source>Current User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="183"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="192"/>
        <source>Can not get the permission info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="322"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="329"/>
        <source>You can not change the access of this file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="335"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="339"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="347"/>
        <source>Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="347"/>
        <source> %1 Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="353"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="360"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="367"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="467"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="468"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="476"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="118"/>
        <source>Show confirm dialog while trashing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="141"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="147"/>
        <source>Origin Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="189"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="198"/>
        <source>Deletion Date: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="168"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="213"/>
        <source>Size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="207"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="214"/>
        <source>Original Location: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="203"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="98"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="98"/>
        <source>file folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="98"/>
        <source>image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="99"/>
        <source>video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="99"/>
        <source>text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="99"/>
        <source>audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="99"/>
        <source>others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="99"/>
        <source>wps file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarCloudItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-cloud-item.cpp" line="40"/>
        <source>CloudStorage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-cloud-item.cpp" line="55"/>
        <source>CloudFile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="84"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="87"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="95"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="189"/>
        <source>KmreData</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="169"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="64"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="87"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="113"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="128"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="299"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="349"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="98"/>
        <source>Delete Symbolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="144"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="152"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="343"/>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="161"/>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="207"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="240"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="276"/>
        <source>burndata</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="99"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="66"/>
        <source>notify</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">Yeni Pencerede Aç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="339"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="221"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="270"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="273"/>
        <source>Forbid Thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="280"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="287"/>
        <source>Resident in Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="300"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="306"/>
        <source>&amp;About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="308"/>
        <source>Peony Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="309"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
</context>
<context>
    <name>Peony::UserShareInfoManager</name>
    <message>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="111"/>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="144"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="160"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="774"/>
        <source>starting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="871"/>
        <source>canceling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="873"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation type="unfinished">Simge Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation type="unfinished">Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="184"/>
        <source>Can not trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="178"/>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="182"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="274"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="278"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="283"/>
        <source>Do you want to empty the recycle bin and delete the files permanently? Once it has begun there is no way to restore them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="340"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="342"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="340"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="342"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="obsolete">Aygıtı çıkarma başarısız oldu</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="obsolete">Lütfen cihazın dolu olup olmadığını kontrol edin ve ardından cihazı tekrar çıkarın</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="525"/>
        <source>Format failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="527"/>
        <source>YES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1003"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1017"/>
        <source>qmesg_notify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1001"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1017"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1030"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1001"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1003"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1032"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1034"/>
        <source>begin format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1036"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="63"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="930"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="139"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1660"/>
        <source>Force unmount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="139"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1660"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="523"/>
        <source>Error: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="142"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1663"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1713"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1692"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1695"/>
        <source>Unmount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1680"/>
        <source>Not authorized to perform operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1692"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1185"/>
        <source>The device has been mount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1372"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1405"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1680"/>
        <source>Eject failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1175"/>
        <source>Failed to activate device: Incorrect passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1377"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1411"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1695"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="265"/>
        <source>favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="267"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="301"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="306"/>
        <source>File is not existed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="314"/>
        <source>Share Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="318"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="322"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="368"/>
        <source>Operation not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="462"/>
        <source>The virtual file system does not support folder creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="534"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="541"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="553"/>
        <source>Can not create symbolic file here, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="562"/>
        <source>Can not add a file to favorite directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="620"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="628"/>
        <source>The virtual file system cannot be opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="449"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="477"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="492"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="578"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="596"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="33"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="407"/>
        <source>permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="400"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="413"/>
        <source>file not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="379"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="396"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="412"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="605"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="158"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="180"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="202"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="209"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="225"/>
        <source>duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="348"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="186"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="351"/>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="250"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="255"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="393"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="404"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="58"/>
        <source>burn operation has been cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="62"/>
        <source> is busy!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfAppendBurnDataDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="23"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="36"/>
        <source>Disc Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="44"/>
        <source>Device Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="57"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="59"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="67"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <source>No burn data, please add!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="164"/>
        <source>Sorry, the appendBurnData operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="165"/>
        <source>Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="174"/>
        <source>Burning. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="183"/>
        <source>Burning this disc will append datas on it. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="184"/>
        <source>Burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="186"/>
        <source>Begin Burning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="187"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="23"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="141"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="191"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="36"/>
        <source>Disc Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="44"/>
        <source>Device Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="58"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="68"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="106"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="106"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="141"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="146"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="147"/>
        <source>Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="181"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="190"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="193"/>
        <source>Begin Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="194"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
<context>
    <name>UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="18"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="139"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="189"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="31"/>
        <source>Disc Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="39"/>
        <source>Device Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="53"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="55"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="63"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="102"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="102"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="139"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="144"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="145"/>
        <source>Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="179"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="188"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="191"/>
        <source>Begin Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="192"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
</TS>
