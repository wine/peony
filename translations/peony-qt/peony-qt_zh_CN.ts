<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>窗口</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Offical Website: </source>
        <translation type="vanished">官方网站: </translation>
    </message>
    <message>
        <source>Service &amp; Technology Support: </source>
        <translation type="vanished">服务与技术支持: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="47"/>
        <location filename="../../src/windows/about-dialog.cpp" line="168"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持团队: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="46"/>
        <location filename="../../src/windows/about-dialog.cpp" line="98"/>
        <location filename="../../src/windows/about-dialog.cpp" line="114"/>
        <source>Peony</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <source>peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="51"/>
        <location filename="../../src/windows/about-dialog.cpp" line="129"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation>文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>Hot Service: </source>
        <translation type="vanished">服务热线: </translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="50"/>
        <location filename="../../src/windows/about-dialog.cpp" line="117"/>
        <source>Version number: %1</source>
        <translation>版本号: %1</translation>
    </message>
    <message>
        <source>File manager is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="vanished">文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">无</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>编辑颜色</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="84"/>
        <source>Delete</source>
        <translation>删除标记</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="89"/>
        <source>Create New Label</source>
        <translation>创建标记</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">打开终端</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="137"/>
        <source>Go Back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="147"/>
        <source>Go Forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="205"/>
        <source>View Type</source>
        <translation>视图类型</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="260"/>
        <source>Sort Type</source>
        <translation>排序类型</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="751"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="394"/>
        <source>Operate Tips</source>
        <translation>操作提示</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="162"/>
        <source>Go Up</source>
        <translation>上一级</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="214"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="290"/>
        <source>&amp;Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="293"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="305"/>
        <source>&amp;Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="308"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="314"/>
        <source>&amp;Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="318"/>
        <location filename="../../src/control/header-bar.cpp" line="330"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="335"/>
        <location filename="../../src/control/header-bar.cpp" line="836"/>
        <location filename="../../src/control/header-bar.cpp" line="854"/>
        <source>Deselect All</source>
        <translation>取消全选</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="340"/>
        <source>&amp;Delete to trash</source>
        <translation>删除到回收站</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="343"/>
        <source>Delete to trash</source>
        <translation>删除到回收站</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="395"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>没有找到任何终端插件，请确认您至少安装了一个！</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="613"/>
        <location filename="../../src/control/header-bar.cpp" line="839"/>
        <location filename="../../src/control/header-bar.cpp" line="845"/>
        <source>Select All Item</source>
        <translation>全部选择</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="623"/>
        <location filename="../../src/control/header-bar.cpp" line="635"/>
        <location filename="../../src/control/header-bar.cpp" line="806"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="598"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="601"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="641"/>
        <source>Select Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="649"/>
        <source>MoveTo</source>
        <translation>移动到</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="658"/>
        <source>CopyTo</source>
        <translation>复制到</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="667"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="733"/>
        <source>Select path</source>
        <translation>选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="766"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="780"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">最大化/还原</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>Intel::NavigationSideBar</name>
    <message>
        <location filename="../../src/control/intel/intel-navigation-side-bar.cpp" line="180"/>
        <source>Open In &amp;New Window</source>
        <translation>在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-navigation-side-bar.cpp" line="204"/>
        <source>Open In New &amp;Tab</source>
        <translation>在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>Intel::TitleLabel</name>
    <message>
        <location filename="../../src/control/intel/intel-navigation-side-bar.cpp" line="516"/>
        <source>Files</source>
        <translation>文件</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="802"/>
        <source>File Manager</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="369"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="376"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="726"/>
        <location filename="../../src/windows/main-window.cpp" line="767"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="726"/>
        <location filename="../../src/windows/main-window.cpp" line="767"/>
        <source>This operation is not supported.</source>
        <translation>不支持此操作。</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="800"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">温馨提示</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">回收站没有文件需要被清空！</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="815"/>
        <source>New Folder</source>
        <translation>新建文件夹</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">所有标记...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="411"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="411"/>
        <source>This operation is not supported.</source>
        <translation>不支持此操作。</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="203"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="449"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="462"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="203"/>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation>设备正忙, 请稍后执行此操作.</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="449"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation>这是个异常U盘，请将其修复或格式化</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="462"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation>这是一个空光驱, 请插入光盘.</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="231"/>
        <source>Open In New Window</source>
        <translation>在新窗口中打开</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="247"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="281"/>
        <source>Can not open %1, %2</source>
        <translation>无法打开%1, %2</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="265"/>
        <source>Open In New Tab</source>
        <translation>在新标签页中打开</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="630"/>
        <source>All tags...</source>
        <translation>所有标记...</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="123"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>在%2中搜索%1</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">高级搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="76"/>
        <source>Keep Allow</source>
        <translation>置顶窗口</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="88"/>
        <source>Show Hidden</source>
        <translation>显示隐藏文件</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="96"/>
        <source>Show File Extension</source>
        <translation>显示文件扩展名</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="101"/>
        <source>Forbid thumbnailing</source>
        <translation>禁用缩略图</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="110"/>
        <source>Resident in Backend</source>
        <translation>常驻后台</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="119"/>
        <source>Parallel Operations</source>
        <translation>允许操作并行</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="125"/>
        <source>Set samba password</source>
        <translation>设置samba共享密码</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="139"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="139"/>
        <source>The user already has a samba password, do you need to reset the samba password?</source>
        <translation>用户已经设置了samba共享密码, 是否希望重新设置?</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="146"/>
        <source>Samba set user password</source>
        <translation>设置samba共享密码</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="146"/>
        <source>Samba password:</source>
        <translation>Samba密码:</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="151"/>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="151"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>设置Samba密码失败, 请重新输入!</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共享配置服务异常, 请确认是否有已经存在的共享设置操作, 或者重新设置共享!</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="177"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="227"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="238"/>
        <source>copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="247"/>
        <source>paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="256"/>
        <source>cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="265"/>
        <source>trash</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarCloudItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-cloud-item.cpp" line="40"/>
        <source>CloudStorage</source>
        <translation>云存储</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-cloud-item.cpp" line="55"/>
        <source>CloudFile</source>
        <translation>云文件</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarFavoriteItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-favorite-item.cpp" line="46"/>
        <source>Favorites</source>
        <translation>快速访问</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarFileSystemItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="60"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="79"/>
        <source>文件系统</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="80"/>
        <source>System Disk</source>
        <translation>系统盘</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarMenu</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="51"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="70"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="96"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="111"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="173"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="241"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="81"/>
        <source>Delete Symbolic</source>
        <translation>删除链接</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="131"/>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="233"/>
        <source>Unmount</source>
        <translation>卸载</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="150"/>
        <source>Eject</source>
        <translation>弹出</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-menu.cpp" line="202"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarPersonalItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-personal-item.cpp" line="45"/>
        <source>Personal</source>
        <translation>个人</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarSeparatorItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-separator-item.h" line="70"/>
        <source>(No Sub Directory)</source>
        <translation>(无子目录)</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::SideBarUserDiskItem</name>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="722"/>
        <source>User Disk</source>
        <translation>用户盘</translation>
    </message>
</context>
<context>
    <name>Peony::Intel::TabletSideBarFactory</name>
    <message>
        <location filename="../../src/control/intel/tablet-side-bar-factory.cpp" line="60"/>
        <source>Intel Side Bar</source>
        <translation>Intel侧边栏</translation>
    </message>
</context>
<context>
    <name>Peony::SearchWidget</name>
    <message>
        <location filename="../../src/control/search-widget.cpp" line="28"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>Peony::TrashWarnDialog</name>
    <message>
        <location filename="../../src/windows/trash-warn-dialog.cpp" line="101"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <location filename="../../src/windows/trash-warn-dialog.cpp" line="114"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../src/windows/trash-warn-dialog.cpp" line="115"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="163"/>
        <source>peony-qt</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="170"/>
        <source>Files or directories to open</source>
        <translation>需要打开的文件或文件夹</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="170"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[文件1，文件2...]</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="211"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="211"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>文件管理器无法获取系统图标主题，可能的原因是：

1.正在以管理员用户运行文件管理器，虽然这意味着你拥有更高的权限，但是你必须了解你同时也失去了一些在普通用户下才能够拥有的特征，例如“我的电脑”以及系统主题。如果不是特殊情况，请不要使用管理员用户打开文件管理器。

2.你使用的系统主题不是qt默认支持的主题，并且你没有安装相关的平台插件。如果你正在使用Gtk主题作为系统主题，尝试安装qt5-gtk2-platformtheme以解决此问题。</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="470"/>
        <source>Peony Qt</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="471"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation>作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2020, 麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>关闭所有窗口并退出</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>打开文件所在目录并选中它们</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>显示文件夹下的内容</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>打开文件属性窗口</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not open %1.</source>
        <translation type="vanished">无法打开 %1.</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="451"/>
        <source>Force unmount failed</source>
        <translation>强制卸载失败</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="451"/>
        <source>Error: %1
</source>
        <translation>错误：%1 </translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="455"/>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="494"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>数据同步完成，设备已经安全卸载！</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="473"/>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="478"/>
        <source>Unmount failed</source>
        <translation>卸载失败</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="473"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>无法卸载设备，可以尝试先关闭一些应用程序，比如GParted等。</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="478"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>错误：%1 
您确定要强制卸载吗？</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="630"/>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="684"/>
        <source>Eject failed</source>
        <translation>弹出失败</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="631"/>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="685"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="632"/>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="686"/>
        <source>Eject Anyway</source>
        <translation>强制弹出</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="644"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation>数据同步完成，设备可以安全移除了！</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="682"/>
        <source>Unable to eject %1</source>
        <translation>无法弹出 %1</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="700"/>
        <source>PeonyNotify</source>
        <translation>文件管理器消息</translation>
    </message>
    <message>
        <location filename="../../src/control/intel/intel-side-bar-file-system-item.cpp" line="701"/>
        <source>File Manager</source>
        <translation>文件管理器</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation>原始路径</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation>使用全局排序</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>Current path has:</source>
        <translation type="vanished">当前路径包含：</translation>
    </message>
    <message>
        <source>%1 folders, %2 files</source>
        <translation type="vanished">%1 文件夹，%2 文件</translation>
    </message>
    <message>
        <source>%1 folders</source>
        <translation type="vanished">%1 文件夹</translation>
    </message>
    <message>
        <source>%1 files</source>
        <translation type="vanished">%1 文件</translation>
    </message>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 个文件, 共%2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 个文件, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">选中%1个</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="173"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>在%2中搜索%1</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="106"/>
        <source> %1 items </source>
        <translation> %1 个项目 </translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="161"/>
        <source>selected%1%2</source>
        <translation>选中%1%2</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="190"/>
        <source> selected %1 items    %2</source>
        <translation> 选中 %1 个项目    %2</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="192"/>
        <source> %1 items    selected %2 items</source>
        <translation>%1 项   选中 %2 项</translation>
    </message>
    <message>
        <source> selected %1 items</source>
        <translation type="vanished"> 选中 %1 个项目</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="218"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="222"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="227"/>
        <source>Recover</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="341"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <source>Close Filter.</source>
        <translation type="vanished">关闭筛选。</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="vanished">筛选</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="410"/>
        <source>Select Path</source>
        <translation>选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1547"/>
        <source>Open failed</source>
        <translation>打开失败</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1548"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>打开文件夹失败，您没有该目录的权限！</translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">关闭高级搜索。</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="331"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Choose other path to search.</source>
        <translation type="vanished">选择其他搜索路径。</translation>
    </message>
    <message>
        <source>Search recursively</source>
        <translation type="vanished">递归搜索</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">更多选项</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">显示/隐藏高级搜索</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="468"/>
        <location filename="../../src/control/tab-widget.cpp" line="634"/>
        <source>is</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="511"/>
        <source>Please input key words...</source>
        <translation>请输入关键词...</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">请输入关键词...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="595"/>
        <location filename="../../src/control/tab-widget.cpp" line="618"/>
        <source>contains</source>
        <translation>包含</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="294"/>
        <source>name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="294"/>
        <source>type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="294"/>
        <source>modify time</source>
        <translation>修改时间</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="294"/>
        <source>file size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="295"/>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>all</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="295"/>
        <source>file folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="295"/>
        <source>image</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="295"/>
        <source>video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="296"/>
        <source>text file</source>
        <translation>文本文档</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="296"/>
        <source>audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="296"/>
        <source>others</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="296"/>
        <source>wps file</source>
        <translation>WPS文件</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <source>today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <source>this week</source>
        <translation>本周</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <source>this month</source>
        <translation>本月</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <source>this year</source>
        <translation>今年</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="297"/>
        <source>year ago</source>
        <translation>一年前</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>tiny(0-16K)</source>
        <translation>极小(0-16K)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>small(16k-1M)</source>
        <translation>很小(16k-1M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>empty(0K)</source>
        <translation>空(0K)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>medium(1M-128M)</source>
        <translation>中等(1M-128M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>big(128M-1G)</source>
        <translation>大(128M-1G)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>large(1-4G)</source>
        <translation>巨大(1-4G)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="298"/>
        <source>great(&gt;4G)</source>
        <translation>极大(&gt;4G)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">很大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>TitleLabel</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="753"/>
        <source>Peony</source>
        <translation>文件管理器</translation>
    </message>
</context>
<context>
    <name>TopMenuBar</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="1007"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="1020"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="1043"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
</TS>
