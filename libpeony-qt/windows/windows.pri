INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/FMWindowIface.h \
    $$PWD/fm-window-factory.h \
    $$PWD/fm-window.h \
    $$PWD/format_dialog.h \
    $$PWD/ky-udf-format-dialog.h \
    $$PWD/properties-window.h \
    $$PWD/udfFormatDialog.h \
    $$PWD/udfAppendBurnDataDialog.h

SOURCES += \
    $$PWD/fm-window-factory.cpp \
    $$PWD/fm-window.cpp \
    $$PWD/format_dialog.cpp \
    $$PWD/ky-udf-format-dialog.cpp \
    $$PWD/properties-window.cpp \
    $$PWD/udfFormatDialog.cpp \
    $$PWD/udfAppendBurnDataDialog.cpp

FORMS += \
    $$PWD/format_dialog.ui
